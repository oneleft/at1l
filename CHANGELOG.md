#AdvancedTomato-OneLeft **[AT1L]**
-----------------------
##version 1 - *2016-04-22*
* **Busybox:** Added exfat volume id support
* **PHP:** Enabled filter
* **Exfat-nofuse:** Added exfat support with external kernel module
* **rc:** Added support for 'usb_fs_exfat' nvram entry
* **shared:** Added exfat volume id support and default 'usb_fs_exfat' nvram value
* **NginX:** added more complete list of mime types
* **h5ai:** Added h5ai for auto index
* **shared:** Added support for 'nginx_ai' nvram entry
* **www:** Added gui controls for exfat and h5ai
* **h5ai:** Added ability to set ROOT_PATH in fastcgi_param
* **NginX:** Modified conf creation to allow for h5ai to be declared as a location