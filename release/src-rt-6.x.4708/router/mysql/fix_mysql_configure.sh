#!/bin/bash
#
# Date: 2016-04-24
# Author: oneleft
# Purpose: find and replace cross compile errors in mysql configure script 
# Why? Had to write this isht because direct call to sed from makefile fails
#

sed -i ':a;N;$!ba;s/as_fn_error $? "cannot run test program while cross compiling\nSee .`config.log. for more details. .$LINENO. 5/$as_echo "skipping..."/g' configure