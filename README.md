![logo](http://files.zpivot.com/at1l/at1l-color-100.svg)
# AdvancedTomato-OneLeft **[AT1L]**
----
[![Shippable branch](https://img.shields.io/shippable/571bce222a8192902e1db714/prod.svg?maxAge=2592000?style=flat-square)](https://app.shippable.com/projects/571bce222a8192902e1db714)

----
This project's goal is to add additional features out of the box to the advancedtomato-arm firmware byJacky Prahec.  The following additional features are the roadmap for this project:

- **[x]** exFat

- **[x]** h5ai

- **[x]** build files corrections

- **[x]** shippable auto build and deploy to bitbucket downloads section

- **[_]** smartmontools gui

- **[_]** mysql upgrade

- **[_]** samba upgrade

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

It is recommended that you have one of the following OSes installed to follow the build instructions in the following section:

* Debian Wheezy 7.7
* Xubuntu 16.04

*__Note:__ You may be able to adapt the build instructions for other linux distributions*

### Building


```
A step by step series of examples that tell you have to get a development env running
will be added very soon for both linux distributions in the above section.
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository. 

## Authors

* **Zach Washburn** - *Initial work* - [oneleft](https://bitbucket.org/oneleft)

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Shibby
* Jacksi
* Victek
* Roadkill
* *And many more who have generously given their time to building tomato...*