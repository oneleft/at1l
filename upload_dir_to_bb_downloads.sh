#!/bin/bash
#
# Author: oneleft
# Date: 2016-04-25
# Purpose: To upload files to Bitbucket downloads area of repo 
# Why? needed for image extraction from shippable ci container after successful build
# Usage: $ ./upload_dir_to_bb_downloads.sh {dir_to_upload} {full_repo_name}
#
for file in $1/*
do
  curl -v -u $BB_USER:$BB_PW -X POST https://api.bitbucket.org/2.0/repositories/$2/downloads -F files=@$file;
done;